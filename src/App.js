import './App.css';
import Todos from './pages/Todos';
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">Redux Saga</header>
        <Todos />
      </div>
    </Provider>
  );
}

export default App;
